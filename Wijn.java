/*
	Verbeteringen:
		* In methode setLand gebruik boolean voor het zoeken van een geldig landnaam vervangen
		door het land te initialiseren op "onbekend" en te overschrijven bij het vinden van een landnaam in de array.
		* De argumenten die worden meegegeven aan de klasse vanuit de main in de juist volgorde plaatsen
		* De operator "<" in de vergelijking met de laagstePrijs tov. het doorlopen item van de wijn-arraylist omkeren
		=> de laagste prijs mag enkel overschreven worden als de doorlopen prijs lager is.

*/

package be.pxl.project1.wijnen;

import java.time.LocalDate;

public class Wijn {
	private String naam;
	private String omschrijving;
	private String druivenRas;
	private String kleur;
	private String land;
	private String regio;
	private int jaargang;
	private double alcoholPercentage;
	private String serveertip;
	private String[] landen = {"Frankrijk", "Chili", "Itali�", "Spanje"};
	private double[] basisPrijzen = {7.50, 8.25, 9.30, 6.45};
	public static final double MAX_ALCOHOLPERCENTAGE = 17; 
	//MAX_ALCOHOLPERCENTAGE is public door tests JUNIT
	
	public Wijn(String naam, String kleur, String land) {
		this(naam, kleur, land, LocalDate.now().getYear()-1, 12.5);
	}
	
	public Wijn(String naam, String kleur, String land, int jaargang, double alcoholPercentage) {
		this.naam = naam;
		setLand(land);
		setKleur(kleur);
		setJaargang(jaargang);
		setAlcoholPercentage(alcoholPercentage);
	}
	
	public void setLand(String land) {

		this.land = "onbekend";					//Indien geen land gevonden blijft de waarde "onbekend"
											    //behouden -> geen nood aan boolean
		for(String landnaam : landen) {
			if(land.equals(landnaam)) {
				this.land = landnaam;	
			}
		}
		
		
	}
	
	public void setKleur(String kleur) {
		
		if(kleur.equals("Wit") || kleur.equals("Ros�")){
			this.kleur = kleur;
		} else {
			this.kleur = "Rood";
		}
		
	}
	
	public String getNaam() {
		return naam;
	}

	public String getOmschrijving() {
		return omschrijving;
	}

	public String getDruivenras() {
		return druivenRas;
	}

	public String getKleur() {
		return kleur;
	}

	public String getLand() {
		return land;
	}

	public String getRegio() {
		return regio;
	}

	public int getJaargang() {
		return jaargang;
	}

	public double getAlcoholPercentage() {
		return alcoholPercentage;
	}

	public String getServeerTip() {
		return serveertip;
	}

	public void setNaam(String naam) {
		this.naam = naam;
	}
	
	public void setJaargang(int jaargang) {
		jaargang = Math.max(1950, jaargang);
		jaargang = Math.min(LocalDate.now().getYear(), jaargang);
		
		this.jaargang = jaargang;
	}
	
	public void setAlcoholPercentage(double alcoholPercentage) {
		alcoholPercentage = Math.max(0, alcoholPercentage);
		alcoholPercentage = Math.min(MAX_ALCOHOLPERCENTAGE, alcoholPercentage);
		
		this.alcoholPercentage = alcoholPercentage;
	}

	public void setOmschrijving(String omschrijving) {
		this.omschrijving = omschrijving;
	}

	public void setDruivenras(String druivenRas) {
		this.druivenRas = druivenRas;
	}

	public void setRegio(String regio) {
		this.regio = regio;
	}

	public void setServeerTip(String serveertip) {
		this.serveertip = serveertip;
	}
	
	public double getAdviesPrijs() {
		double adviesPrijs;
		int indexBasisPrijs = 0;
		boolean gevonden = false;
		
		for(int i=0; i < landen.length; i++) {
			if(land.equals(landen[i])) {
				indexBasisPrijs = i;
				gevonden = true;
			}
		}
		if(gevonden) {
			adviesPrijs = basisPrijzen[indexBasisPrijs];
		} else {
			adviesPrijs = berekenHoogstBasisAdviesPrijs();
		}
		
		
		if(!String.format("%d", jaargang).endsWith("7")) { // Enkel als het niet eindigt op 7
			for(int jaar=jaargang; jaar < LocalDate.now().getYear(); jaar++) {
				adviesPrijs += 0.10;
			}
		}
		
		adviesPrijs = Math.round(adviesPrijs * 100.0) / 100.0; //afronding 2 decimalen
		
		return adviesPrijs;
	}
	
	public double berekenHoogstBasisAdviesPrijs() {
		double hoogste = 0;
		for(double basisprijs : basisPrijzen) {
			if(hoogste < basisprijs) {
				hoogste = basisprijs;
			}
		}
		return hoogste;
	}
	
	public String getLabel() {
		StringBuilder label = new StringBuilder();
		
		label.append(naam + "\n");
		label.append(druivenRas + "\n");
		label.append(regio + "\n");
		label.append(jaargang + "\n");
		
		return label.toString();
	}
	
	public String toString() {
		StringBuilder output = new StringBuilder();
		
		output.append(getLabel());
		output.append(omschrijving + "\n");
		output.append(kleur + "\n");
		output.append(alcoholPercentage + "\n");
		output.append(land + "\n");
		output.append(serveertip + "\n");
		
		return output.toString();
	}
}
