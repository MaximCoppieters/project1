package be.pxl.project1.wijnen;

import java.util.ArrayList;

public class WijnApp {

	public static void main(String[] args) {
		
		ArrayList<Wijn> wijnen = new ArrayList<>();
		
		Wijn parra = new Wijn("Parra", "Wit", "Spanje", 2016, 11.5);
		Wijn jardine = new Wijn("Domaine de la Jardine", "Rood", "Frankrijk", 2016, 14);
		Wijn montebello= new Wijn("Montebello Grande Bellezza", "Rood", "Itali�", 2016, 13);
		Wijn palmer = new Wijn("Ch�teau Palmer 3eme Grand Cru", "Rood" , "Frankrijk", 2002, 13.5);
		//Volgorde constructor respecteren in main bij aanpassing in klasse!
		parra.setOmschrijving("Licht, fris en strak");
		parra.setDruivenras("Sauvignon Blanc");
		parra.setRegio("La Mancha");
		parra.setServeerTip("Visgerechten");
		
		jardine.setOmschrijving("Licht, soepel en fruitig");
		jardine.setDruivenras("Merlot");
		jardine.setRegio("Pays d'Oc");
		jardine.setServeerTip("Gevogelte");
		
		montebello.setOmschrijving("Licht, soepel en fruitig");
		montebello.setDruivenras("Negroamaro");
		montebello.setRegio("Puglia");
		montebello.setServeerTip("Gegrild vlees");
		
		palmer.setOmschrijving("Complex, krachtig en vol");
		palmer.setDruivenras("Merlot");
		palmer.setRegio("Bordeaux");
		
		wijnen.add(parra);
		wijnen.add(jardine);
		wijnen.add(montebello);
		wijnen.add(palmer);
		
		int aantalRode = 0;
		int duursteIndex=0;
		double hoogstePrijs = 0;
		int goedkoopsteIndex=0;
		double laagstePrijs = 10000;
		ArrayList<Wijn> wijnenGevogelte = new ArrayList<>();
		
		for(int i=0; i < wijnen.size(); i++) {
			if(wijnen.get(i).getAdviesPrijs() > hoogstePrijs) {
				hoogstePrijs = wijnen.get(i).getAdviesPrijs();
				duursteIndex = i;
			}
			if(laagstePrijs > wijnen.get(i).getAdviesPrijs()) { //laagstePrijs moet groter zijn!
				laagstePrijs = wijnen.get(i).getAdviesPrijs();
				goedkoopsteIndex = i;
			}
			if(wijnen.get(i).getKleur().equals("Rood")) {
				aantalRode++;
			}
			
			if(wijnen.get(i).getServeerTip() != null && wijnen.get(i).getServeerTip().equals("Gevogelte")) {
				wijnenGevogelte.add(wijnen.get(i));
			}

		}
		
		System.out.println("** Duurste fles:");
		System.out.print(wijnen.get(duursteIndex).getLabel());
		System.out.println(wijnen.get(duursteIndex).getAdviesPrijs());
		System.out.println("** Goedkoopste fles:");
		System.out.print(wijnen.get(goedkoopsteIndex).getLabel());
		System.out.println(wijnen.get(goedkoopsteIndex).getAdviesPrijs());
		System.out.println("** Aantal flessen rode wijn: ");
		System.out.println(aantalRode);
		System.out.println("** Suggestie(s) bij gevogelte:");
		for(Wijn wijn : wijnenGevogelte) {
			System.out.print(wijn.getLabel());
		}
	}
}